#include<clc.h>
__kernel void __attribute__ ((reqd_work_group_size(55,1,1)))fire3_squeeze(__global float *fire2_Features,__global float *fire3squeeze1x1_Weights_HW, __global float *fire3squeeze1x1_Features)
{

	int x = get_local_id(0);
	int y = get_group_id(0);

	float Features1 = 0;

	for(int f=0; f<16; f++)
	{
		Features1 = 0;
		for(int n=0; n<128; n++)
		{
               		Features1+= fire2_Features[n*55*55 + x*55 + y]*fire3squeeze1x1_Weights_HW[f*128+n];
		}
		//ReLU activation function computation
		if(Features1<0)
			Features1 = 0;
		fire3squeeze1x1_Features[f*55*55 + x*55 + y] = Features1;
	}

}
